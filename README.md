# Mandrill Template Generator

This project is a tool to design HTML/Text email templates for the Mandrill
platform. It allows you to design with SASS stylesheets and handlebars
templates, viewing the compiled preview live in your browser. Then when you're
ready to move the code over to Mandill just copy your template from the
output folder.

if you are changing json, you need to restart gulp to see the changes
