var gulp = require ('gulp'),
    inlineCss = require ('gulp-inline-css'),
    sass = require ('gulp-sass'),
    livereload = require ('gulp-livereload'),
    handlebars = require('gulp-compile-handlebars'),
    rename = require('gulp-rename'),
    fs = require ('fs')

var templates = [
  "template"
]

templates.forEach (function (template) {
  gulp.task ('sass-' + template, function () {
    return gulp.src ('./input/scss/' + template + '.scss')
      .pipe (sass ().on ('error', sass.logError))
      .pipe (gulp.dest ('./input/tmp'))
  })

  gulp.task ('inline-' + template, ['sass-' + template], function() {
      return gulp.src ('input/' + template + '.handlebars')
          .pipe (inlineCss ())
          .pipe (gulp.dest ('output/'))
  })

  gulp.task ('handlebars-' + template, ['inline-' + template], function () {
    var data = require ('./input/json/' + template + '.json')

    return gulp.src ('output/' + template + '.handlebars')
      .pipe (handlebars (data))
      .pipe (rename (template + '.html'))
      .pipe (gulp.dest ('preview'))
      .pipe (livereload ())
  })

  gulp.task ('watch-' + template, function () {
    gulp.watch (
      [
        './input/scss/' + template + '.scss',
        './input/scss/global.scss',
        './input/json/' + template + '.json',
        './input/' + template + '.handlebars',
      ],
      ['handlebars-' + template]
    )
  })
})

gulp.task ('default', templates.map (t => 'watch-' + t), function () {
  livereload.listen ()
})
